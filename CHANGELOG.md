# Changelog

## [Unreleased](https://github.com/buluma/ansible-role-haproxy/tree/HEAD)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.4.4...HEAD)

**Merged pull requests:**

- Bump buluma/galaxy-action from 1.0.3 to 6.25.22 [\#56](https://github.com/buluma/ansible-role-haproxy/pull/56) ([dependabot[bot]](https://github.com/apps/dependabot))

## [v2.4.4](https://github.com/buluma/ansible-role-haproxy/tree/v2.4.4) (2022-05-13)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.4.3...v2.4.4)

**Merged pull requests:**

- Testing [\#54](https://github.com/buluma/ansible-role-haproxy/pull/54) ([buluma](https://github.com/buluma))
- Bump buluma/gh-action-auto-merge-dependabot-updates from 1.0.4 to 1.0.5 [\#52](https://github.com/buluma/ansible-role-haproxy/pull/52) ([dependabot[bot]](https://github.com/apps/dependabot))

## [v2.4.3](https://github.com/buluma/ansible-role-haproxy/tree/v2.4.3) (2022-04-24)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.4.2...v2.4.3)

**Closed issues:**

- Cannot detect the required Python library cryptography \(\>= 1.2.3\)"} [\#51](https://github.com/buluma/ansible-role-haproxy/issues/51)

## [v2.4.2](https://github.com/buluma/ansible-role-haproxy/tree/v2.4.2) (2022-03-20)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.4.1...v2.4.2)

## [v2.4.1](https://github.com/buluma/ansible-role-haproxy/tree/v2.4.1) (2022-03-10)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.4.0...v2.4.1)

**Merged pull requests:**

- Bump codacy/codacy-analysis-cli-action from 4.0.0 to 4.0.2 [\#49](https://github.com/buluma/ansible-role-haproxy/pull/49) ([dependabot[bot]](https://github.com/apps/dependabot))

## [v2.4.0](https://github.com/buluma/ansible-role-haproxy/tree/v2.4.0) (2022-03-03)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.3.9...v2.4.0)

**Merged pull requests:**

- set concurrency [\#47](https://github.com/buluma/ansible-role-haproxy/pull/47) ([buluma](https://github.com/buluma))

## [v2.3.9](https://github.com/buluma/ansible-role-haproxy/tree/v2.3.9) (2022-02-13)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.3.8...v2.3.9)

**Merged pull requests:**

- Bump codacy/codacy-analysis-cli-action from 1.1.0 to 4.0.0 [\#45](https://github.com/buluma/ansible-role-haproxy/pull/45) ([dependabot[bot]](https://github.com/apps/dependabot))

## [v2.3.8](https://github.com/buluma/ansible-role-haproxy/tree/v2.3.8) (2022-02-13)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.3.7...v2.3.8)

**Fixed bugs:**

- Build Failed [\#41](https://github.com/buluma/ansible-role-haproxy/issues/41)
- Build Failed [\#40](https://github.com/buluma/ansible-role-haproxy/issues/40)

**Closed issues:**

- Needs to be updated [\#42](https://github.com/buluma/ansible-role-haproxy/issues/42)

**Merged pull requests:**

- Testing [\#44](https://github.com/buluma/ansible-role-haproxy/pull/44) ([buluma](https://github.com/buluma))
- Update .gitlab-ci.yml [\#43](https://github.com/buluma/ansible-role-haproxy/pull/43) ([buluma](https://github.com/buluma))

## [v2.3.7](https://github.com/buluma/ansible-role-haproxy/tree/v2.3.7) (2022-02-01)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.3.6...v2.3.7)

## [v2.3.6](https://github.com/buluma/ansible-role-haproxy/tree/v2.3.6) (2022-02-01)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.3.4...v2.3.6)

**Fixed bugs:**

- Build Failed [\#39](https://github.com/buluma/ansible-role-haproxy/issues/39)
- Build Failed [\#38](https://github.com/buluma/ansible-role-haproxy/issues/38)
- Build Failed [\#37](https://github.com/buluma/ansible-role-haproxy/issues/37)
- Build Failed [\#36](https://github.com/buluma/ansible-role-haproxy/issues/36)
- Build Failed [\#35](https://github.com/buluma/ansible-role-haproxy/issues/35)
- Build Failed [\#34](https://github.com/buluma/ansible-role-haproxy/issues/34)
- Build Failed [\#33](https://github.com/buluma/ansible-role-haproxy/issues/33)
- Build Failed [\#32](https://github.com/buluma/ansible-role-haproxy/issues/32)
- Build Failed [\#31](https://github.com/buluma/ansible-role-haproxy/issues/31)
- Build Failed [\#30](https://github.com/buluma/ansible-role-haproxy/issues/30)
- Build Failed [\#29](https://github.com/buluma/ansible-role-haproxy/issues/29)
- Build Failed [\#28](https://github.com/buluma/ansible-role-haproxy/issues/28)
- Build Failed [\#27](https://github.com/buluma/ansible-role-haproxy/issues/27)
- Build Failed [\#26](https://github.com/buluma/ansible-role-haproxy/issues/26)
- Build Failed [\#25](https://github.com/buluma/ansible-role-haproxy/issues/25)
- Build Failed [\#24](https://github.com/buluma/ansible-role-haproxy/issues/24)
- Build Failed [\#23](https://github.com/buluma/ansible-role-haproxy/issues/23)
- Build Failed [\#22](https://github.com/buluma/ansible-role-haproxy/issues/22)
- Build Failed [\#21](https://github.com/buluma/ansible-role-haproxy/issues/21)
- Build Failed [\#20](https://github.com/buluma/ansible-role-haproxy/issues/20)

## [v2.3.4](https://github.com/buluma/ansible-role-haproxy/tree/v2.3.4) (2021-05-29)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v2.3.5...v2.3.4)

## [v2.3.5](https://github.com/buluma/ansible-role-haproxy/tree/v2.3.5) (2021-05-29)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v1.2...v2.3.5)

## [v1.2](https://github.com/buluma/ansible-role-haproxy/tree/v1.2) (2021-04-18)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v1.0...v1.2)

## [v1.0](https://github.com/buluma/ansible-role-haproxy/tree/v1.0) (2021-04-18)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/v1.1...v1.0)

**Merged pull requests:**

- Add .circleci/config.yml [\#19](https://github.com/buluma/ansible-role-haproxy/pull/19) ([buluma](https://github.com/buluma))

## [v1.1](https://github.com/buluma/ansible-role-haproxy/tree/v1.1) (2021-03-28)

[Full Changelog](https://github.com/buluma/ansible-role-haproxy/compare/89a7829d021e17c6e0bcd6f672d22d56880dacc8...v1.1)

**Merged pull requests:**

- update slack [\#18](https://github.com/buluma/ansible-role-haproxy/pull/18) ([buluma](https://github.com/buluma))
- Update .codeclimate.yml [\#17](https://github.com/buluma/ansible-role-haproxy/pull/17) ([buluma](https://github.com/buluma))
- Update Jenkinsfile [\#16](https://github.com/buluma/ansible-role-haproxy/pull/16) ([buluma](https://github.com/buluma))
- Update Jenkinsfile [\#15](https://github.com/buluma/ansible-role-haproxy/pull/15) ([buluma](https://github.com/buluma))
- Buluma jenkins [\#14](https://github.com/buluma/ansible-role-haproxy/pull/14) ([buluma](https://github.com/buluma))
- add group: edge [\#13](https://github.com/buluma/ansible-role-haproxy/pull/13) ([buluma](https://github.com/buluma))
- Create jenkinsfile [\#11](https://github.com/buluma/ansible-role-haproxy/pull/11) ([buluma](https://github.com/buluma))
- Add a Codacy badge to README.md [\#10](https://github.com/buluma/ansible-role-haproxy/pull/10) ([codacy-badger](https://github.com/codacy-badger))
- disable checkmode [\#9](https://github.com/buluma/ansible-role-haproxy/pull/9) ([buluma](https://github.com/buluma))
- checkmodee [\#8](https://github.com/buluma/ansible-role-haproxy/pull/8) ([buluma](https://github.com/buluma))
- print out test file [\#6](https://github.com/buluma/ansible-role-haproxy/pull/6) ([buluma](https://github.com/buluma))
- force verbose of test file [\#3](https://github.com/buluma/ansible-role-haproxy/pull/3) ([buluma](https://github.com/buluma))
- update distros and notifications [\#2](https://github.com/buluma/ansible-role-haproxy/pull/2) ([buluma](https://github.com/buluma))
- First Commit [\#1](https://github.com/buluma/ansible-role-haproxy/pull/1) ([buluma](https://github.com/buluma))



\* *This Changelog was automatically generated by [github_changelog_generator](https://github.com/github-changelog-generator/github-changelog-generator)*
